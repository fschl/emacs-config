;;; fschl-org.el -- Fschl Crafted Emacs user customization file -*- lexical-binding: t; -*-

;; This file is generated from the .org-file with the same name. If you want to edit the
;; configuration, DO NOT edit this .el-file, edit .org, instead.

;; see variable `org-roam-database-connector`
;(add-to-list 'package-selected-packages 'emacsql-sqlite-builtin)
;(add-to-list 'package-selected-packages 'emacsql-sqlite)
;(add-to-list 'package-selected-packages 'org-roam)
;(add-to-list 'package-selected-packages 'org-roam-ui)
(add-to-list 'package-selected-packages 'denote)
(add-to-list 'package-selected-packages 'org-modern)
(add-to-list 'package-selected-packages 'ox-latex)
(add-to-list 'package-selected-packages 'ox-koma-letter)
(package-install-selected-packages :noconfirm)

(with-eval-after-load 'org (global-org-modern-mode))

(setq org-default-notes-file "~/Documents/Org/inbox.org")
(keymap-global-set "C-c o c" #'org-capture)
(keymap-global-set "C-c o l" #'org-store-link)
(keymap-global-set "C-c o a" #'org-agenda)

(keymap-global-set "<f7>" #'org-cycle-agenda-files)
;(keymap-global-set "<f8>" #'find-file org-default-notes-file)

(setq org-agenda-files
      (quote ("~/Documents/Org/inbox.org"
              "~/Documents/Org/journal.org"
              "~/Documents/Org/private.org"
              "~/Documents/Org/projects.org"
              "~/Documents/Org/tasks.org"
              "~/Documents/Org/birthdays.org")))
(setq org-refile-targets '(("~/Documents/Org/projects.org" :maxlevel . 3)
                           ("~/Documents/Org/journal.org" :level . 1)
                           ("~/Documents/Org/private.org" :maxlevel . 2)))

(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
              (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING"))))
(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
              ("NEXT" :foreground "blue" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
              ("WAITING" :foreground "orange" :weight bold)
              ("HOLD" :foreground "magenta" :weight bold)
              ("CANCELLED" :foreground "forest green" :weight bold)
              ("MEETING" :foreground "forest green" :weight bold)
              ("PHONE" :foreground "forest green" :weight bold))))

(setq calendar-week-start-day 1)
(setq calendar-intermonth-text
      '(propertize
        (format "%2d"
                (car
                 (calendar-iso-from-absolute
                  (calendar-absolute-from-gregorian (list month day year)))))
        'font-lock-face 'font-lock-warning-face))

(setq calendar-intermonth-header
      (propertize "KW"                  ; or e.g. "Wk"
                  'font-lock-face 'font-lock-keyword-face))

(eval-after-load 'ox '(require 'ox-koma-letter))
  (eval-after-load 'ox '(require 'ox-moderncv))
  (eval-after-load 'ox '(require 'ox-awesomecv))
  (eval-after-load 'ox-koma-letter
    '(progn
       (add-to-list 'org-latex-classes
                      '("Brief-de-modern"
                        "\\documentclass\[Brief-de-modern\]\{scrlttr2\}
         \[DEFAULT-PACKAGES]
         \[PACKAGES]
         \[EXTRA]"))

         ;; \\usepackage[english]{babel}
         ;; \\setkomavar{frombank}{(1234)\\,567\\,890}

         (setq org-koma-letter-default-class "Brief-de-modern")))

(defun fschl/create-org-letter ()
  "Create a new letter in ~/Documents/letters/ with filename and date"
  (interactive)
  (let ((name (read-string "Filename: ")))
    (expand-file-name (format "%s.org" name) "~/Documents/letters/") ))

;; This is needed as of Org 9.2
(use-package org-tempo
  :ensure nil
  :after org
  :config
  (dolist (item '(("sh" . "src sh")
                  ("el" . "src emacs-lisp")
                  ("li" . "src lisp")
                  ("sc" . "src scheme")
                  ("ts" . "src typescript")
                  ("py" . "src python")
                  ("go" . "src go")
                  ("einit" . "src emacs-lisp :tangle ~/.config/emacs/init.el :mkdirp yes")
                  ("emodule" . "src emacs-lisp :tangle ~/.config/emacs/modules/fschl-MODULE.el :mkdirp yes")
                  ("yaml" . "src yaml")
                  ("json" . "src json")))
    (add-to-list 'org-structure-template-alist item)))

(setq org-capture-templates
      '(("t" "todo list item" entry
         (file+olp+datetree "~/Documents/Org/inbox.org")
         "* TODO %?\n SCHEDULED: %^T"
         :tree-type month
         )
        ("T" "todo list item with source" entry
         (file+olp+datetree "~/Documents/Org/inbox.org")
         "* TODO %?\n %a \n SCHEDULED: %^T \n %^G \n"
         :tree-type month
         )

        ("r" "Todo research some website/software" entry
         (file+olp+datetree "~/Documents/Org/tasks.org")
         "* TODO %?\n  SCHEDULED: %^T \n %^L \n"
         :tree-type month
         )
        ("l" "letter to Documents/letters/<datetime.org>"
         entry (file fschl/create-org-letter)
         "* Preamble :noexport:
\# #+TITLE: ?
\# #+DATE:
\
\#+SUBJECT: Betreff des Briefs
\
\#+LCO: Absender-Frieder
\# #+LCO: Absender-Marcelle
\# #+LCO: Absender-FamilieSchlesier
\#+LCO: Brief-de-modern
\#+STARTUP: showall
\
\* To-address                                                             :to:
\
\# * From :from:
\
\* Sehr geehrte Damen und Herren,
\?
\
\* Mit freundlichen Grüßen,                                          :closing:
\
Frieder Schlesier")
        ("m" "Schedule a meeting" entry
         (file+headline "~/Documents/Org/inbox.org")
         "* MEETING %?\n SCHEDULED: %^T\n %a"
         )
        ("p" "Schedule a phone call" entry
         (file+headline "~/Documents/Org/inbox.org")
         "* PHONE %?\n SCHEDULED: %^T\\n %a"
         )
        ("a" "Articles: keep notes of online articles"
         entry
         (file+olp+datetree "~/Documents/Org/journal.org")
         "* %? \n%x \n %u\n- $?"
         :tree-type month
         :kill-buffer t
         :empty-lines-before 1)
        )
      )

;; active Babel languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '((shell . t)
   (emacs-lisp . t)
   (dot . t)))

(setq org-directory "~/Documents/Org/denotes/")
  (use-package denote
    :bind
    ("C-c n n" . 'denote)
    ("C-c n f" . 'denote-open-or-create)
    ;; ("C-c n f" . 'my/project-org-file-file)
    ("C-c n k" . 'denote-keywords-add)    ;; update file name automatically
    ("C-c n K" . 'denote-keywords-remove) ;; update file name automatically
    ("C-c n u" . 'denote-rename-file-using-front-matter)
    ("C-c n l" . 'denote-link-find-backlink)
    ("C-c n r" . 'my/denote-random-note)
    :init
    (setq denote-directory (expand-file-name org-directory))
    :config
    (setq denote-known-keywords '("emacs"))
    (setq denote-prompts '(subdirectory title))
    (setq denote-excluded-directories-regexp ".attachment")

    ;; Makes the denote links different from usual link.
    (set-face-attribute 'denote-faces-link
                        nil :foreground "magenta" :inherit 'link)

    ;; Remove the date and the identifier. They are duplicated with the file name.
    ;; I want to remove filetags too but denote-keyword-* need that.
    (setq denote-org-front-matter "#+title: %1$s\n#+filetags: %3$s\n")

    (add-hook 'dired-mode-hook #'denote-dired-mode))

(use-package ox-awesomecv
  :load-path "~/src/org-cv/"
  :init (require 'ox-awesomecv))
(use-package ox-moderncv
  :load-path "~/src/org-cv/"
  :init (require 'ox-moderncv))
(setq org-ref-default-bibliography '("~/Documents/references/references.bib")
      org-ref-pdf-directory "~/Documents/references/"
      org-ref-bibliography-notes "~/Documents/references/notes.org")
