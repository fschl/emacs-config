(add-to-list 'package-selected-packages 'org-roam)
(add-to-list 'package-selected-packages 'org-roam-ui)
(add-to-list 'package-selected-packages 'simple-httpd)
(add-to-list 'package-selected-packages 'htmlize)
(add-to-list 'package-selected-packages 'websocket)
(add-to-list 'package-selected-packages 'f)
(package-install-selected-packages :noconfirm)

(require 'htmlize)
(require 'org-roam)
(require 'org-roam-ui)
(require 'ox-publish)

(setq org-export-with-broken-links 'mark)
(setq org-export-html-coding-system 'utf-8-unix)
(setq org-html-doctype "html5")
(defvar fschlde-html-head "<link type='text/css' rel='stylesheet' href='../css/plain.css'>")
(setq fschlde-html-footer " <footer>\nfschl.de 2018-2023\n</footer>")

;; following two funcs are taken from
;; https://www.orgroam.com/manual.html#How-do-I-publish-my-notes-with-an-Internet_002dfriendly-graph_003f
;; sitemap function for my roam-publish
(defun roam-sitemap (title list)
  (concat "#+OPTIONS: ^:nil author:nil html-postamble:nil\n"
          "#+SETUPFILE: ./simple_inline.theme\n"
          "#+HTML_HEAD_EXTRA: <link rel=\"stylesheet\" type=\"text/css\" href=\"../css/plain.css\" />\n"
          "#+TITLE: " title "\n\n"
          (org-list-to-org list) "\nfile:sitemap.svg"))

(setq my-publish-time 0)   ; see the next section for context

;; 
(defun roam-publication-wrapper (plist filename pubdir)
  (org-roam-graph)
  (org-html-publish-to-html plist filename pubdir)
  (setq my-publish-time (cadr (current-time))))

(defun org-roam-custom-link-builder (node)
  (let ((file (org-roam-node-file node)))
    (concat (file-name-base file) ".html")))

(setq org-roam-graph-link-builder 'org-roam-custom-link-builder)

(add-hook 'org-roam-graph-generation-hook
          (lambda (dot svg) (if (< (- (cadr (current-time)) my-publish-time) 5)
                                (progn (copy-file svg "~/projects/blog-fschlde/pkb/sitemap.svg" 't)
                                       (kill-buffer (file-name-nondirectory svg))
                                       (setq my-publish-time 0)))))


(setq org-publish-project-alist
      '(("website"
         :components ("org-journal", "project-notes", "org-sites", "blog-rss",  "blog-images"))
        ("org-journal"
         :base-directory "~/Documents/Org/"
         :base-extension "org"
         :publishing-directory "~/projects/blog-fschlde/reads/"
         :recursive nil
         :publishing-function org-html-publish-to-html

         :headline-levels 4
         :with-author t
         :with-creator nil
         :with-date t

         :with-toc t
         :with-drawers nil

         :html-link-home "/"
         :html-head nil ;; cleans up anything that would have been in there.
         ;; :html-head-extra ,my-blog-extra-head
         :html-head-include-default-style nil
         :html-head-include-scripts nil

         ;; :html-preamble my-blog-header
         ;; :html-postamble ,my-blog-footer
         )
        ("project-notes"
         :base-directory "~/projects/"
         :base-extension "org"
         :publishing-directory "~/projects/blog-fschlde/projects/"
         :recursive nil
         :publishing-function org-html-publish-to-html
         :headline-levels 4)
        ("org-sites"
         :base-directory "~/projects/fschlde-static/"
         :base-extension "org\\|css\\|js\\|ico\\|png\\|html"
         :publishing-directory "~/projects/blog-fschlde/"
         :recursive t
         :publishing-function org-publish-attachment

         :html-link-home "/"
         ;; :html-head ,fschlde-html-head ;; cleans up anything that would have been in there.
         :html-head-include-default-style nil
         :html-head-include-scripts nil
         :with-drawers nil
         :with-creator nil

         ;; :html-head-extra fschlde-html-head
         ;; :html-postamble fschlde-html-footer
         :headline-levels 4)
        ("roam"
         :base-directory "~/org-roam/"
         :auto-sitemap t
         :recursive nil
         :base-extension "org"
         :exclude ".stversions/\\|.stfolder"
         :sitemap-function roam-sitemap
         :sitemap-title "Roam Notes"
         :style "<link rel=\"stylesheet\" href=\"../css/plain.css\" type=\"text/css\">"
         :publishing-function roam-publication-wrapper
         :publishing-directory "~/projects/fschlde-static/pkb/"
         :section-number nil
         :table-of-contents t
         )
        )
      )
