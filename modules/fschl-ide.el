;;; fschl-ide.el -- Fschl Crafted Emacs user customization file -*- lexical-binding: t; -*-
;; This file is generated from the .org-file with the same name. If you want to edit the
;; configuration, DO NOT edit this .el-file, edit .org, instead.

(add-to-list 'package-selected-packages 'cargo)
(add-to-list 'package-selected-packages 'paredit)
(add-to-list 'package-selected-packages 'evil-paredit)
(add-to-list 'package-selected-packages 'rustic)
(add-to-list 'package-selected-packages 'ob-rust)
(add-to-list 'package-selected-packages 'ob-rust)
(package-install-selected-packages :noconfirm)

(use-package paredit :ensure t
             :hook ((emacs-lisp-mode . enable-paredit-mode)
                    (eval-expression-minibuffer-setup . enable-paredit-mode)
     (ielm-mode . enable-paredit-mode)
     (lisp-mode . enable-paredit-mode)
     (lisp-interaction-mode . enable-paredit-mode)
     (scheme-mode . enable-paredit-mode))
             )

(use-package smartparens :ensure t
  :config (require 'smartparens-rust))

(defun sp1ff/rust/mode-hook ()
  "My rust-mode hook"

  (column-number-mode)
  (display-line-numbers-mode)
  (hs-minor-mode)
  (smartparens-mode)
  (define-key rust-mode-map "\C-ca" 'eglot-code-actions)
  (define-key rust-mode-map (kbd "C-<right>")   'sp-forward-slurp-sexp)
  (define-key rust-mode-map (kbd "C-<left>")    'sp-forward-barf-sexp)
  (define-key rust-mode-map (kbd "C-M-<right>") 'sp-backward-slurp-sexp)
  (define-key rust-mode-map (kbd "C-M-<left>")  'sp-backward-barf-sexp)
  (define-key rust-mode-map "\C-c>" 'hs-show-all)
  (define-key rust-mode-map "\C-c<" 'hs-hide-all)
  (define-key rust-mode-map "\C-c-" 'hs-toggle-hiding)
  (define-key rust-mode-map "\C-c+" 'hs-hide-level)
  (setq indent-tabs-mode nil
        tab-width 4
        c-basic-offset 4
        fill-column 100))

(use-package rust-mode
  :ensure t
  :hook (rust-mode . sp1ff/rust/mode-hook)
  :config
  (let ((dot-cargo-bin (expand-file-name "~/.cargo/bin/")))
    (setq rust-rustfmt-bin (concat dot-cargo-bin "rustfmt")
          rust-cargo-bin (concat dot-cargo-bin "cargo")
          rust-format-on-save t)))

(use-package flymake-clippy
  :hook (rust-mode . flymake-clippy-setup-backend))

(defun clippy-flymake-manually-activate-flymake ()
  "Shim for working around eglot's tendency to suppress flymake backends."
  (add-hook 'flymake-diagnostic-functions #'eglot-flymake-backend nil t)
  (flymake-mode 1))

;; `eglot' by default will suppress all other flymake backends than its own
;; <https://github.com/joaotavora/eglot/issues/268> This workaround will
;; add `flymake-clippy'
(use-package eglot
  :ensure t
  :hook ((rust-mode . eglot-ensure)
         (eglot-managed-mode . clippy-flymake-manually-activate-flymake))
  :config
  (add-to-list 'eglot-stay-out-of 'flymake))

(use-package company
:ensure
:custom
(company-idle-delay 0.5) ;; how long to wait until popup
;; (company-begin-commands nil) ;; uncomment to disable popup
:bind
(:map company-active-map
            ("C-n". company-select-next)
            ("C-p". company-select-previous)
            ("M-<". company-select-first)
            ("M->". company-select-last)))

(use-package flycheck :ensure)
(setq lsp-inlay-hint-enable t)
