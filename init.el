;;; init.el -- Fschl Crafted Emacs user customization file -*- lexical-binding: t; -*-
;; This file is generated from the .org-file with the same name. If you want to edit the
;; configuration, DO NOT edit this .el-file, edit .org, instead.

(setq user-emacs-directory "~/.config/emacs/")
(setq crafted-emacs-home (expand-file-name "crafted-emacs/" user-emacs-directory))
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (and custom-file
           (file-exists-p custom-file))
  (load custom-file nil :nomessage))

(load (expand-file-name "modules/crafted-init-config" crafted-emacs-home))

(require 'crafted-completion-packages)
(require 'crafted-evil-packages)
(require 'crafted-org-packages)
(require 'crafted-ui-packages)
(require 'crafted-ide-packages)
(require 'crafted-writing-packages)

(add-to-list 'package-selected-packages 'vterm)
(add-to-list 'package-selected-packages 'all-the-icons-dired)
(add-to-list 'package-selected-packages 'ranger)
(add-to-list 'package-selected-packages 'dired-ranger)
(add-to-list 'package-selected-packages 'smtpmail-multi)

(package-install-selected-packages :noconfirm)

(require 'crafted-defaults-config)
(require 'crafted-startup-config)
(require 'crafted-completion-config)
(require 'crafted-evil-config)
(require 'crafted-org-config)
(require 'crafted-ui-config)
(require 'crafted-ide-config)
(require 'crafted-writing-config)

(require 'vterm)

(unless crafted-startup-inhibit-splash
  (setq initial-buffer-choice #'crafted-startup-screen))

(defvar dw/is-termux
  (string-suffix-p "Android" (string-trim (shell-command-to-string "uname -a"))))

(defvar dw/is-guix-system (and (eq system-type 'gnu/linux)
                               (file-exists-p "/etc/os-release")
                               (with-temp-buffer
                                 (insert-file-contents "/etc/os-release")
                                 (search-forward "ID=guix" nil t))
                               t))

(keymap-global-unset "M-#")

(define-key global-map (kbd "M-#") 'comment-line)
(define-key global-map (kbd "s-b") 'switch-to-buffer)
(define-key global-map (kbd "s-w") 'kill-current-buffer)
(define-key global-map (kbd "s-W") 'kill-buffer-and-window)
(define-key global-map (kbd "s-o") 'other-window)
(define-key global-map (kbd "C-z") nil)

(use-package all-the-icons-dired)
(use-package dired-ranger)

(defun dw/dired-mode-hook ()
  (interactive)
  (dired-hide-details-mode 0)
  (unless (or dw/is-termux
              (string-equal "/gnu/store/" (expand-file-name default-directory)))
    (all-the-icons-dired-mode 1))
  (hl-line-mode 1))


(use-package dired
  :ensure nil
  :bind (:map dired-mode-map
              ("b" . dired-up-directory)
              ("H" . dired-hide-details-mode))
  :config
  (setq dired-listing-switches "-agho --group-directories-first"
        dired-omit-files "^\\.[^.].*"
        dired-omit-verbose nil
        dired-dwim-target 'dired-dwim-target-next
        dired-hide-details-hide-symlink-targets nil
        dired-kill-when-opening-new-dired-buffer t
        delete-by-moving-to-trash t)

  (add-hook 'dired-mode-hook #'dw/dired-mode-hook)
  (global-set-key (kbd "s-j") #'dired-jump))

(require 'smtpmail)
(use-package smtpmail-multi
  :ensure t
  :config
    (setq smtpmail-multi-accounts
    '(
      (fschl. ("frieder@fschl.de" "mx2f14.netcup.net" 465 "frieder@fschl.de" nil nil nil nil))
      (gmx-main . ("f.schlesier@gmx.de" "smtp.gmx.net" 587 "f.schlesier@gmx.de" nil nil nil nil))))

    (setq smtpmail-multi-associations
    '(
      ("frieder@fschl.de" fschl)
      ("f.schlesier@gmx.de" gmx-main)))

    (setq smtpmail-multi-default-account 'fschl)
    (setq message-send-mail-function 'smtpmail-multi-send-it)

    (setq smtpmail-debug-info t)
    (setq smtpmail-debug-verbose t))

(load (expand-file-name "modules/fschl-ide.el" user-emacs-directory))
(load (expand-file-name "modules/fschl-org.el" user-emacs-directory))
